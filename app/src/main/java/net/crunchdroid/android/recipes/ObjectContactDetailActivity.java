package net.crunchdroid.android.recipes;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

public class ObjectContactDetailActivity extends AppCompatActivity {
    private final String TAG = getClass().getSimpleName();
    private TextView tvFirstName;
    private TextView tvLastName;
    private TextView tvPhone;
    private TextView tvEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_detail);

        tvFirstName = (TextView) findViewById(R.id.tv_first_name);
        tvLastName = (TextView) findViewById(R.id.tv_last_name);
        tvPhone = (TextView) findViewById(R.id.tv_phone);
        tvEmail = (TextView) findViewById(R.id.tv_email);

        Contact contact = (Contact) getIntent().getSerializableExtra("contact");

        tvFirstName.setText(contact.getFirstName());
        tvLastName.setText(contact.getLastName());
        tvPhone.setText(contact.getPhone());
        tvEmail.setText(contact.getEmail());

    }
}
