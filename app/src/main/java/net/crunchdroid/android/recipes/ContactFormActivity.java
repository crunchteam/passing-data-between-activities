package net.crunchdroid.android.recipes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class ContactFormActivity extends AppCompatActivity {

    private final String TAG = getClass().getSimpleName();
    private EditText etFirstName;
    private EditText etLastName;
    private EditText etPhone;
    private EditText etEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_contact_form);

        etFirstName = (EditText) findViewById(R.id.et_first_name);
        etLastName = (EditText) findViewById(R.id.et_last_name);
        etPhone = (EditText) findViewById(R.id.et_phone);
        etEmail = (EditText) findViewById(R.id.et_email);
    }

    public void onClickHandler(View view) {

        String firstName = etFirstName.getText().toString();
        String lastName = etLastName.getText().toString();
        String phone = etPhone.getText().toString();
        String email = etEmail.getText().toString();

        Intent intent;

        switch (view.getId()) {
            case R.id.btn_submit_data:
                intent = new Intent(this, DataContactDetailActivity.class);
                intent.putExtra("EXTRA_FIRST_NAME", firstName);
                intent.putExtra("EXTRA_LAST_NAME", lastName);
                intent.putExtra("EXTRA_PHONE", phone);
                intent.putExtra("EXTRA_EMAIL", email);

                break;
            case R.id.btn_submit_bundle:
                intent = new Intent(this, BundleContactDetailActivity.class);
                Bundle extras = new Bundle();
                extras.putString("EXTRA_FIRST_NAME", firstName);
                extras.putString("EXTRA_LAST_NAME", lastName);
                extras.putString("EXTRA_PHONE", phone);
                extras.putString("EXTRA_EMAIL", email);
                intent.putExtras(extras);
                break;
            default:
                intent = new Intent(this, ObjectContactDetailActivity.class);
                Contact contact = new Contact(firstName, lastName, phone, email);
                intent.putExtra("contact", contact);
                break;
        }


        startActivity(intent);
    }
}
