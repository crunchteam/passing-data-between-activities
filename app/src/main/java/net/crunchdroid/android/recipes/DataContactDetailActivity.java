package net.crunchdroid.android.recipes;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class DataContactDetailActivity extends AppCompatActivity {

    private final String TAG = getClass().getSimpleName();
    private TextView tvFirstName;
    private TextView tvLastName;
    private TextView tvPhone;
    private TextView tvEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_detail);

        tvFirstName = (TextView) findViewById(R.id.tv_first_name);
        tvLastName = (TextView) findViewById(R.id.tv_last_name);
        tvPhone = (TextView) findViewById(R.id.tv_phone);
        tvEmail = (TextView) findViewById(R.id.tv_email);

        tvFirstName.setText(getIntent().getStringExtra("EXTRA_FIRST_NAME"));
        tvLastName.setText(getIntent().getStringExtra("EXTRA_LAST_NAME"));
        tvPhone.setText(getIntent().getStringExtra("EXTRA_PHONE"));
        tvEmail.setText(getIntent().getStringExtra("EXTRA_EMAIL"));

    }
}
